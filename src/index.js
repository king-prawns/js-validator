const t = require('io-ts')
const isRight = require('fp-ts/lib/Either').isRight

const Venue = t.type({
  venue1: t.string,
  venue2: t.number,
  venue3: t.boolean
})

const City = t.type({
  city1: t.string,
  city2: t.array(Venue)
})

const Country = t.type({
  country1: t.string,
  country2: t.array(City)
})

const Letter = t.type({
  letter1: t.array(Country)
})

const Sitemap = t.record(t.string, Letter)

const sitemapMock =
{
  'a': {
      'letter1': [
          {
              'country1': 'contry 1 string',
              'country2': [
                  {
                      'city1': 'city 1 string',
                      'city2': [
                          {
                              'venue1': 'venue 1 string',
                              'venue2': 1,
                              'venue3': true
                          },
                          {
                              'venue1': 'venue 1 string',
                              'venue2': 1,
                              'venue3': true
                          },
                          {
                              'venue1': 'venue 1 string',
                              'venue2': 1,
                              'venue3': true
                          },
                      ]
                  }
              ]
          }
      ] 
  },
  'b': {
      'letter1': [
          {
              'country1': 'contry 1 string',
              'country2': [
                  {
                      'city1': 'city 1 string',
                      'city2': [
                          {
                              'venue1': 'venue 1 string',
                              'venue2': 1,
                              'venue3': true
                          },
                          {
                              'venue1': 'venue 1 string',
                              'venue2': 1,
                              'venue3': true
                          },
                          {
                              'venue1': 'venue 1 string',
                              'venue2': 1,
                              'venue3': true
                          },
                      ]
                  }
              ]
          }
      ] 
  },
  'c': {
      'letter1': [
          {
              'country1': 'contry 1 string',
              'country2': [
                  {
                      'city1': 'city 1 string',
                      'city2': [
                          {
                              'venue1': 'venue 1 string',
                              'venue2': 1,
                              'venue3': true
                          },
                          {
                              'venue1': 'venue 1 string',
                              'venue2': 1,
                              'venue3': true
                          },
                          {
                              'venue1': 'venue 1 string',
                              'venue2': 1,
                              'venue3': true
                          },
                      ]
                  }
              ]
          }
      ] 
  },
}
const sitemap = Sitemap.decode(sitemapMock);

const result = isRight(sitemap) 
console.log (result)